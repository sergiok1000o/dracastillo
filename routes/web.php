<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', function () {
    return view('login');
});
Route::get('/controlpanel', function () {
    return view('controlpanel');
});

Route::get('/', 'MethodController@index')->name('index');
/*store table*/
Route::post('/storecomment', 'MethodController@storeComment')->name('storeComment');
Route::post('/storeappointment', 'MethodController@storeAppointment')->name('storeAppointment');
Route::post('/storeschedule', 'MethodController@storeSchedule')->name('storeSchedule');
Route::post('/deletechedule', 'MethodController@deleteSchedule')->name('deleteSchedule');
/*list table*/
Route::post('/comments/list', 'MethodController@commentList')->name('comment.list');
Route::post('/appointments/list', 'MethodController@appointmentList')->name('appointments.list');
Route::post('/schedules/list', 'MethodController@scheduleList')->name('schedules.list');

Route::get('/home', 'MethodController@index')->name('home');

Auth::routes(['register' => false]);
