<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{ asset('files/Corazon.png') }}">
    <title>Panel administrativo</title>
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" />

    <link rel="stylesheet" href="{{ asset('js/plugins/tableTech/listTable.css')}}">
    <link rel="stylesheet" href="{{ asset('js/plugins/tableTech/pluginsTableTech.css')}}">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="{{ asset('plugins/css/font-awesome/font-awesome.min.css?v4.0.2')}}">
    <link rel="stylesheet" href="{{ asset('plugins/css/web-icons/web-icons.min.css')}}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">

</head>
<body>



    <div id="wrapper">
        <div class="navbar navbar-inverse" style="margin:0px;height:10%">
            <div class="adjust-nav" style="height: 100%;">
                <div class="navbar-header" style="height: 100%;">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#" style="height: 100%;">
                        <img src="{{ asset('files/Corazon.png') }}" style="height:100%"/>
                    </a>
                </div>
                <span class="logout-spn">
                    <a href="/login" style="color:#fff;font-size:15px">LOGOUT</a>
                </span>
            </div>
        </div>
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                     <li class="active-link" id="mensajes-link">
                        <a onclick="changeWindow('mensajes')"><i class="fa fa-desktop "></i>Mensajes</a>
                    </li>
                    <li id="citas-link">
                        <a onclick="changeWindow('citas')"><i class="fa fa-table "></i>Citas agendadas</a>
                    </li>
                    <li id="horarios-link">
                        <a onclick="changeWindow('horarios')"><i class="fa fa-edit "></i>Horarios</a>
                    </li>
                </ul>
            </div>
        </nav>
        <div id="page-wrapper" >
            <div id="page-inner">

                <div id="mensajes">
                    <h2>Mensajes</h2>
                    <div class="table-tech" id="tableComments">
                        <div class="panel-body" id="tableTech">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-15">
                                        <div class="btn-group" aria-label="Button group with nested dropdown" role="group">
                                            <div class="btn-group" role="group">
                                                <button type="button" class="btn btn-outline btn-default dropdown-toggle" id="exampleGroupDrop2"
                                                    data-toggle="dropdown" aria-expanded="false">
                                                    <i class="icon wb-check-mini" aria-hidden="true"></i>
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="exampleGroupDrop1" role="menu">
                                                    <a class="dropdown-item filter_select_1" href="javascript:void(0)" role="menuitem">Todos</a>
                                                    <a class="dropdown-item filter_select_0" href="javascript:void(0)" role="menuitem">Ninguno</a>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-primary filter_refresh ladda-button" data-style="slide-right" data-plugin="ladda">
                                            <i class="icon wb-refresh" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                </div>
                                <!--<div class="col-md-6">
                                    <div class="mb-15">
                                    <div class="form-group">
                                        <div class="input-search">
                                            <button type="submit" class="input-search-btn"><i class="icon wb-search" aria-hidden="true"></i></button>
                                            <input type="text" class="form-control filter_search" name="" placeholder="Buscar...">
                                        </div>
                                    </div>
                                    </div>
                                </div>-->
                            </div>
                            <table class="table is-indent" cellspacing="0" id="exampleAddRow">
                            </table>
                            <div class="menu_footer">
                                <div class="btn-group" role="group">
                                    <button type="button" class="btn btn-outline btn-default dropdown-toggle filter_row" id="exampleGroupDrop2"
                                    data-toggle="dropdown" aria-expanded="false" value="10">10
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="exampleGroupDrop1" role="menu">
                                    <a class="dropdown-item" href="javascript:void(0)" role="menuitem" value="10">10</a>
                                    <a class="dropdown-item" href="javascript:void(0)" role="menuitem" value="25">25</a>
                                    <a class="dropdown-item" href="javascript:void(0)" role="menuitem" value="50">50</a>
                                    <a class="dropdown-item" href="javascript:void(0)" role="menuitem" value="100">100</a>
                                    </div>
                                </div>
                                <div class="paginate-total"></div>
                                    <div class="btn-group float-right" aria-label="Button group with nested dropdown" role="group">
                                        <div class="btn-group paginate btn-group-sm" role="group">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <script class="template-list" type="text/x-custom-template">
                                <tr>
                                    <td>#_name_#</td>
                                    <td>#_email_#</td>
                                    <td>#_phone_#</td>
                                    <td>#_commnet_#</td>
                                    <td>#_created_at_#</td>
                                </tr>
                            </script>
                        </div>
                    </div>
                <div id="citas" class="d-none">
                        <h2>Citas agendadas</h2>
                        <div class="table-tech" id="tableAppointments">
                            <div class="panel-body" id="tableTech">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="mb-15">
                                            <div class="btn-group" aria-label="Button group with nested dropdown" role="group">
                                                <div class="btn-group" role="group">
                                                    <button type="button" class="btn btn-outline btn-default dropdown-toggle" id="exampleGroupDrop2"
                                                        data-toggle="dropdown" aria-expanded="false">
                                                        <i class="icon wb-check-mini" aria-hidden="true"></i>
                                                    </button>
                                                    <div class="dropdown-menu" aria-labelledby="exampleGroupDrop1" role="menu">
                                                        <a class="dropdown-item filter_select_1" href="javascript:void(0)" role="menuitem">Todos</a>
                                                        <a class="dropdown-item filter_select_0" href="javascript:void(0)" role="menuitem">Ninguno</a>
                                                    </div>
                                                </div>
                                                <button type="button" class="btn btn-primary filter_refresh ladda-button" data-style="slide-right" data-plugin="ladda">
                                                <i class="icon wb-refresh" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <!--<div class="col-md-6">
                                        <div class="mb-15">
                                        <div class="form-group">
                                            <div class="input-search">
                                                <button type="submit" class="input-search-btn"><i class="icon wb-search" aria-hidden="true"></i></button>
                                                <input type="text" class="form-control filter_search" name="" placeholder="Buscar...">
                                            </div>
                                        </div>
                                        </div>
                                    </div>-->
                                </div>
                                <table class="table is-indent" cellspacing="0" id="exampleAddRow">
                                </table>
                                <div class="menu_footer">
                                    <div class="btn-group" role="group">
                                        <button type="button" class="btn btn-outline btn-default dropdown-toggle filter_row" id="exampleGroupDrop2"
                                        data-toggle="dropdown" aria-expanded="false" value="10">10
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="exampleGroupDrop1" role="menu">
                                        <a class="dropdown-item" href="javascript:void(0)" role="menuitem" value="10">10</a>
                                        <a class="dropdown-item" href="javascript:void(0)" role="menuitem" value="25">25</a>
                                        <a class="dropdown-item" href="javascript:void(0)" role="menuitem" value="50">50</a>
                                        <a class="dropdown-item" href="javascript:void(0)" role="menuitem" value="100">100</a>
                                        </div>
                                    </div>
                                    <div class="paginate-total"></div>
                                        <div class="btn-group float-right" aria-label="Button group with nested dropdown" role="group">
                                            <div class="btn-group paginate btn-group-sm" role="group">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <script class="template-list" type="text/x-custom-template">
                                    <tr>
                                        <td>#_name_#</td>
                                        <td>#_phone_#</td>
                                        <td>#_task_date_#</td>
                                        <td>#_schedule_name_#</td>
                                    </tr>
                                </script>
                            </div>
                        </div>
                        <div id="horarios" class="d-none">
                            <h2>Horarios</h2>
                            <div class="table-tech" id="tableSchedules">
                                <div class="panel-body" id="tableTech">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="mb-15">
                                                <div class="btn-group" aria-label="Button group with nested dropdown" role="group">
                                                    <div class="btn-group" role="group">
                                                        <button type="button" class="btn btn-outline btn-default dropdown-toggle" id="exampleGroupDrop2"
                                                            data-toggle="dropdown" aria-expanded="false">
                                                            <i class="icon wb-check-mini" aria-hidden="true"></i>
                                                        </button>
                                                        <div class="dropdown-menu" aria-labelledby="exampleGroupDrop1" role="menu">
                                                            <a class="dropdown-item filter_select_1" href="javascript:void(0)" role="menuitem">Todos</a>
                                                            <a class="dropdown-item filter_select_0" href="javascript:void(0)" role="menuitem">Ninguno</a>
                                                        </div>
                                                    </div>
                                                    <button type="button" class="btn btn-primary filter_refresh ladda-button" data-style="slide-right" data-plugin="ladda">
                                                    <i class="icon wb-refresh" aria-hidden="true"></i>
                                                    <button type="button" class="btn btn-success ladda-button" data-style="slide-right" data-plugin="ladda" data-modal="#modalCreateSchedule" onclick="schedulesTable.modal(this);">
                                                        <i class="icon wb-plus-circle" aria-hidden="true"></i>Crea un horario
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <!--<div class="col-md-6">
                                            <div class="mb-15">
                                            <div class="form-group">
                                                <div class="input-search">
                                                    <button type="submit" class="input-search-btn"><i class="icon wb-search" aria-hidden="true"></i></button>
                                                    <input type="text" class="form-control filter_search" name="" placeholder="Buscar...">
                                                </div>
                                            </div>
                                            </div>
                                        </div>-->
                                    </div>
                                    <table class="table is-indent" cellspacing="0" id="exampleAddRow">
                                    </table>
                                    <div class="menu_footer">
                                        <div class="btn-group" role="group">
                                            <button type="button" class="btn btn-outline btn-default dropdown-toggle filter_row" id="exampleGroupDrop2"
                                            data-toggle="dropdown" aria-expanded="false" value="10">10
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="exampleGroupDrop1" role="menu">
                                            <a class="dropdown-item" href="javascript:void(0)" role="menuitem" value="10">10</a>
                                            <a class="dropdown-item" href="javascript:void(0)" role="menuitem" value="25">25</a>
                                            <a class="dropdown-item" href="javascript:void(0)" role="menuitem" value="50">50</a>
                                            <a class="dropdown-item" href="javascript:void(0)" role="menuitem" value="100">100</a>
                                            </div>
                                        </div>
                                        <div class="paginate-total"></div>
                                            <div class="btn-group float-right" aria-label="Button group with nested dropdown" role="group">
                                                <div class="btn-group paginate btn-group-sm" role="group">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <script class="template-list" type="text/x-custom-template">
                                        <tr>
                                            <td>#_day_name_#</td>
                                            <td>#_time_interval_#</td>
                                            <td>#_status_name_#</td>
                                            <td>
                                                <a class="btn btn-sm btn-icon btn-pure btn-default on-default" onclick="deleteSchedule(#_id_#,1)">
                                                    <i class="icon fa-check-circle" aria-hidden="true"></i>
                                                </a>
                                                <a class="btn btn-sm btn-icon btn-pure btn-default on-default" onclick="deleteSchedule(#_id_#,0)">
                                                    <i class="icon fa-trash" aria-hidden="true"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    </script>
                                </div>
                                @include('partial.modalCreateSchedule')
                            </div>
                        </div>

        </div>
        <div class="footer">
             <div class="row" style="margin:0px;padding:0px">
                <div class="col-lg-12" >
                    &copy;  2019 dramilenacastillo.com
                </div>
            </div>
        </div>


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

    <script href="{{ asset('js/jquery-1.10.2.js') }}"></script>
    <script href="{{ asset('js/bootstrap.min.js') }}"></script>
    <script href="{{ asset('js/custom.js') }}"></script>

    <script src="{{ asset('js/plugins/tableTech/pluginsTableTech.js')}}"></script>
    <script src="{{ asset('js/plugins/tableTech/tableTech.js')}}"></script>

    <script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

    <script src="{{ asset('js/controlPanel.js') }}"></script>
</body>
</html>
