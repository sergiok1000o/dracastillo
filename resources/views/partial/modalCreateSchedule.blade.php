<div class="modal fade" id="modalCreateSchedule">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                <span>×</span>
                </button>
                <h4>Recibir traslado</h4>
            </div>
            <div class="modal-body">
                <form style="margin:auto" id='scheduleForm'>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label class="form-control-label" for="inputBasicFirstName">Día de la semana</label>
                            <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                            <select class="form-control" data-plugin="select2" data-placeholder="Selecciona un módulo" data-allow-clear="true" name="day">
                                <option value="1">Lunes</option>
                                <option value="2">Martes</option>
                                <option value="3">Miercoles</option>
                                <option value="4">Jueves</option>
                                <option value="5">Viernes</option>
                                <option value="6">Sábado</option>
                                <option value="0">Domingo</option>
                            </select>
                            <div class="error"></div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="form-control-label" for="inputBasicLastName">Descripción del documento</label>
                            <div class='input-group date' id='datetimepicker3'>
                                <input type='text' class="form-control" name="time_interval"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-outline receiver" onclick="storeSchedule()">Crear</button>
                <button type="button" class="btn btn-primary btn-outline received" data-dismiss="modal">Cerrar</button>
                <div class="overlay">
                    <div class="content">
                        <div class="loader vertical-align-middle loader-tadpole"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
