<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="icon" href="{{ asset('files/Corazon.png') }}">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css' />
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <title>Dra. Castillo</title>
    </head>
    <body>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                <img class="img-logo" src="{{ asset('files/logo.jpg') }}" style="width: 100%;height: auto;">
                <img class="img-logo-mobile" src="{{ asset('files/logomob.jpg') }}" style="width: 100%;height: auto;">
            </div>
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                <div class="topnav" id="myTopnav">
                    <a href="#dramilenacastillo" class="active">DRA. CASTILLO</a>
                    <a href="#servicios">SERVICIOS</a>
                    <a href="#conoce">LO QUE DEBES SABER COMO PACIENTE</a>
                    <a href="#agendate">AGENDA TU CITA EN LINEA</a>
                    <a href="#contacto">CONTACTO</a>
                    <a href="javascript:void(0);" class="icon" onclick="myFunction()">
                    <i class="fa fa-bars"></i>
                    </a>
                </div>
            </div>
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                <div>
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1"></li>
                            <li data-target="#myCarousel" data-slide-to="2"></li>
                            <li data-target="#myCarousel" data-slide-to="3"></li>
                        </ol>

                        <div class="carousel-inner">
                            <div class="item active">
                                <img class="" src="{{ asset('files/Masthead_1_02.jpg') }}" style="width: 100%;height: auto;">
                            </div>
                            <div class="item">
                                <img class="" src="{{ asset('files/Masthead_2_02.jpg') }}" style="width: 100%;height: auto;">
                            </div>
                            <div class="item">
                                <img class="" src="{{ asset('files/Masthead_3_02.jpg') }}" style="width: 100%;height: auto;">
                            </div>
                            <div class="item">
                                <img class="" src="{{ asset('files/Masthead_4_02.jpg') }}" style="width: 100%;height: auto;">
                            </div>
                        </div>

                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
                <div class="row menu-bar">
                    <div class="col-md-4 col-lg-4 col-sm-4 col-xs-2"></div>
                    <div class="col-md-4 col-lg-4 col-sm-4 col-xs-8" style="padding:0px">
                        <div class="row">
                            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4 menu-top" href="" target="_blank">
                                <a href="https://bit.ly/2TE1vsv" target="_blank"><img src="{{ asset('files/Boton_1.jpg') }}" style="width: 100%;height: auto;"></a>
                            </div>
                            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4 menu-top">
                                <a href="https://www.instagram.com/doctoraendocrino/?igshid=erepextem2w3" target="_blank"><img src="{{ asset('files/Boton_2.jpg') }}" style="width: 100%;height: auto;"></a>
                            </div>
                            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4 menu-top">
                                <a href="https://www.facebook.com/dramilenacastillo/" target="_blank"><img src="{{ asset('files/Boton_3.jpg') }}" style="width: 100%;height: auto;"></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-4 col-xs-2"></div>
                </div>
            </div>
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="dramilenacastillo">
                <img class="img-description" src="{{ asset('files/Doctora_Castillo_02.jpg') }}" style="width: 100%;height: auto;">
                <img class="img-description-mobile" src="{{ asset('files/Doctora_Castillo_MOB.jpg') }}" style="width: 100%;height: auto;">
            </div>
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 divisor">
                <img class="" src="{{ asset('files/Servicios_1.jpg') }}" style="width: 100%;height: auto;">
            </div>
            <div class="col-md-1 col-lg-1 col-sm-1 col-xs-1"></div>
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-5" id="servicios">
                <img class="" src="{{ asset('files/Servicios_2.jpg') }}" style="width: 100%;height: auto;">
            </div>
            <div class="col-md-7 col-lg-7 col-sm-7 col-xs-6"></div>
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 intro" style="padding: 25px 25px;">
                <img class="img-info" src="{{ asset('files/Servicios_3.jpg') }}" style="width: 100%;height: auto;">
                <img class="img-info-mobile" src="{{ asset('files/Servicios_MOB.jpg') }}" style="width: 100%;height: auto;">
            </div>
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 divisor">
                <img class="" src="{{ asset('files/Servicios_1.jpg') }}" style="width: 100%;height: auto;">
            </div>
            <div class="col-md-1 col-lg-1 col-sm-1 col-xs-1"></div>
            <div class="col-md-7 col-lg-7 col-sm-7 col-xs-10" id="conoce">
                <img class="" src="{{ asset('files/Nombre.jpg') }}" style="width: 100%;height: auto;">
            </div>
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-1"></div>

            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" style="margin: 25px 0px;">
                <div class="row">

                    <div class="col-md-1 col-lg-1 col-sm-1 col-xs-1 informate-mobile"></div>
                    <div class="col-md-10 col-lg-10 col-sm-10 col-xs-10 informate-mobile">
                        <div class="topnav2" id="myTopnav2">
                            <a id="info_selecciona" class="active">SELECCIONA</a>
                            <a onClick="changeInfo(this)" id="info_obesidad">OBESIDAD</a>
                            <a onClick="changeInfo(this)" id="info_glandula">GLÁNDULA TIROIDES</a>
                            <a onClick="changeInfo(this)" id="info_paratiroides">GLÁNDULA PARATIROIDES</a>
                            <a onClick="changeInfo(this)" id="info_diabetes">DIABETES MELLITUS</a>
                            <a onClick="changeInfo(this)" id="info_hipertension">HIPERTENSIÓN ARTERIAL</a>
                            <a onClick="changeInfo(this)" id="info_acromegalia">ACROMEGALIA</a>
                            <a onClick="changeInfo(this)" id="info_colesterol">COLESTEROL ALTO</a>
                            <a onClick="changeInfo(this)" id="info_hiperendo">HIPERTENSIÓN DE ORIGEN ENDOCRINO</a>
                            <a onClick="changeInfo(this)" id="info_osteoporosis">OSTEOPOROSIS</a>
                            <a href="javascript:void(0);" class="icon" onclick="myFunction2()">
                            <i class="fa fa-bars"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-1 col-lg-1 col-sm-1 col-xs-1 informate-mobile"></div>
                    <div class="col-md-1 col-lg-1 col-sm-1 col-xs-1 informate"></div>
                    <div class="col-md-10 col-lg-10 col-sm-10 col-xs-10 informate buttons-line">
                        <img class="button-med info_selected" src="{{ asset('files/Boton_Obesidad_03.png') }}" style="width: auto;height: 100%;">
                        <img onClick="changeInfo(this)" id="info_glandula" class="button-med" src="{{ asset('files/Boton_Glandula_Tiroides_04.png') }}" style="width: auto;height: 100%;">
                        <img onClick="changeInfo(this)" id="info_paratiroides" class="button-med" src="{{ asset('files/Boton_Glandula_Paratiroides_05.png') }}" style="width: auto;height: 100%;">
                        <img onClick="changeInfo(this)" id="info_diabetes" class="button-med" src="{{ asset('files/Boton_Diabetes_Mellitus_06.png') }}" style="width: auto;height: 100%;">
                        <img onClick="changeInfo(this)" id="info_hipertension" class="button-med" src="{{ asset('files/Boton_Hipertension_Arterial_07.png') }}" style="width: auto;height: 100%;">
                        <img onClick="changeInfo(this)" id="info_acromegalia" class="button-med" src="{{ asset('files/Boton_Acromegalia_08.png') }}" style="width: auto;height: 100%;">
                        <img onClick="changeInfo(this)" id="info_colesterol" class="button-med" src="{{ asset('files/Boton_Colesterol_Alto_09.png') }}" style="width: auto;height: 100%;">
                        <img onClick="changeInfo(this)" id="info_hiperendo" class="button-med" src="{{ asset('files/Boton_Hipertension_Endocrina_10.png') }}" style="width: auto;height: 100%;">
                        <img onClick="changeInfo(this)" id="info_osteoporosis" class="button-med" src="{{ asset('files/Boton_Osteoporosis_11.png') }}" style="width: auto;height: 100%;">
                    </div>
                    <div class="col-md-1 col-lg-1 col-sm-1 col-xs-1 informate"></div>
                </div>
            </div>
            <div class="col-md-1 col-lg-1 col-sm-1 col-xs-0"></div>
            <div class="col-md-5 col-lg-5 col-sm-5 col-xs-12" id="info_texto">
                La obesidad es una enfermedad crónica que ha  aumentado en   frecuencia  en la población adolescente y adulta siendo considerada una epidemia global.  Se trata de una acumulación de grasa excesiva en el cuerpo  que lleva a   causar problemas  metabólicos, Hormonales y Articulares.<br><br> Padecer de obesidad es un problema, dado  que aumenta el riesgo de tener  muchos problemas de salud. También  causa problemas para moverse, respirar y hacer actividades que otras personas con peso normal realizan con facilidad.<br><br>Existen medicamentos y cirugías que  pueden ayudar a perder peso, pero estos tratamientos están indicados solo para personas con problemas  importantes de peso que no han sido capaces   de perderlo a través de dieta y ejercicio. <br><br>Sin embargo, es importante  entender que estos tratamientos no reemplazan  los hábitos de vida saludable.  Las personas que se someten a estos tratamientos deben cambiar su manera de comer y ser activos, si no los resultados no son duraderos.
            </div>
            <div class="col-md-5 col-lg-5 col-sm-5 col-xs-12 info_img">
                <img id="info_foto" src="{{ asset('files/info_acromegalia.jpg') }}" style="width: 100%;height: auto;">
            </div>
            <div class="col-md-1 col-lg-1 col-sm-1 col-xs-0"></div>


            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 divisor" id="agendate">
                <img class="" src="{{ asset('files/Servicios_1.jpg') }}" style="width: 100%;height: auto;">
            </div>

            <div class="col-md-1 col-lg-1 col-sm-1 col-xs-1"></div>
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-6">
                <a href="http://dramilenacastillo.agendapro.com.co" target="_blank"><img class="" src="{{ asset('files/Agendate_1.jpg') }}" style="width: 85%;height: auto;"></a>
                   <!--<img class="" src="{{ asset('files/Agendate_1.jpg') }}" style="width: 85%;height: auto;" data-toggle="modal" data-target="#myModal">-->
            </div>
            <div class="col-md-0 col-lg-0 col-sm-0 col-xs-5"></div>
            <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12" style="padding-bottom: 30px;">
                <!--<div id='calendar'></div>-->
                <iframe src="https://agendapro.com/iframe/overview/TFdlRlo3Uml2ZXdyaE13RlJWUllwZz09LS0xMFZSUzhqYnBHZmM0SWJwVFV6bStRPT0=--f83c1037be4f6722f433d531b8798aef6c69c0de" width="810" marginwidth="0" marginheight="0" frameborder="no" scrolling="yes" style="border-width:0px;height: 570px;width: 100%;" ></iframe>
            </div>
            <div class="col-md-1 col-lg-1 col-sm-1 col-xs-1"></div>

            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="contacto">
                <img class="con" src="{{ asset('files/Contacto_01.jpg') }}" style="width: 100%;height: auto;">
                <img class="con-mobile" src="{{ asset('files/Contacto_01_MOB.jpg') }}" style="width: 100%;height: auto;">
            </div>

            <div class="col-md-1 col-lg-1 col-sm-1 col-xs-1"></div>
            <div class="col-md-11 col-lg-11 col-sm-11 col-xs-11 dejame">
                DEJAME TUS DATOS Y ESTARÉ EN CONTACTO CONTIGO
            </div>
            <div class="col-md-1 col-lg-1 col-sm-1 col-xs-1"></div>
            <div class="col-md-6 col-lg-6 col-sm-6 col-xs-10">
                <form action="" style="margin:auto" id='contactForm'>
                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                    <div class="input-container">
                        <img class="icon" src="{{ asset('files/Apellido.jpg') }}">
                        <input class="input-field" type="text" placeholder="Escribe tu nombre completo" name="name">
                    </div>
                    <div class="row" style="margin:0px">
                        <div class="input-container col-md-6 col-lg-6 col-sm-6 col-xs-12">
                            <img class="icon" src="{{ asset('files/Email.jpg') }}">
                            <input class="input-field" type="text" placeholder="Correo electrónico" name="email">
                        </div>
                        <div class="input-container col-md-6 col-lg-6 col-sm-6 col-xs-12">
                            <img class="icon" src="{{ asset('files/Telefono.jpg') }}">
                            <input class="input-field" type="text" placeholder="Teléfono" name="phone">
                        </div>
                    </div>
                    <div class="input-container">
                        <img class="icon" src="{{ asset('files/Mensaje.jpg') }}" style="max-height: 50px;">
                        <textarea rows="4" cols="50" class="input-field" placeholder="Dime que información deseas" name="commnet"></textarea>
                    </div>
                </form>
                <img class="input-img" src="{{ asset('files/Contacto_07.jpg') }}" onclick="storeComment()">
            </div>

            <div class="col-md-5 col-lg-5 col-sm-5 col-xs-1"></div>
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                <img class="contacto" src="{{ asset('files/Contacto_08.jpg') }}" style="width: 100%;height: auto;">
                <img class="contacto-mobile" src="{{ asset('files/Contacto_02_MOB.jpg') }}" style="width: 100%;height: auto;">
            </div>

            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 conv">
                <img class="" src="{{ asset('files/convenios_02.jpg') }}" style="width: 100%;height: auto;margin: 40px 0px;">
            </div>
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 conv-mobile">
                    <img class="" src="{{ asset('files/Convenios_MOB_15.jpg') }}" style="width: 100%;height: auto;margin: 40px 0px;">
                </div>
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                <img class="footer" src="{{ asset('files/Pata_02.jpg') }}" style="width: 100%;height: auto;">
                <img class="footer-mobile" src="{{ asset('files/Pata_MOB.jpg') }}" style="width: 100%;height: auto;">
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" style="position:absolute;top: 0%;height: 100%;">
                    <div class="row" style="height: 100%;">
                        <div class="col-md-1 col-lg-1 col-sm-1 col-xs-0"></div>
                        <div class="col-md-2 col-lg-2 col-sm-2 col-xs-5 menu-bot">
                            <a href="https://www.facebook.com/dramilenacastillo/" target="_blank"><img class="img-bot" src="{{ asset('files/Redes_Pata_1.png') }}"></a>
                            <a href="https://www.instagram.com/doctoraendocrino/?igshid=erepextem2w3" target="_blank"><img class="img-bot" src="{{ asset('files/Redes_Pata_2.png') }}"></a>
                            <a><img class="img-bot" src="{{ asset('files/Redes_Pata_3.png') }}"></a>
                            <a href="https://www.linkedin.com/in/milena-castillo-mejia-61441699"><img class="img-bot" src="{{ asset('files/Redes_Pata_4.png') }}"></a>
                        </div>
                        <div class="col-md-9 col-lg-9 col-sm-9 col-xs-7 menu-bot"></div>
                    </div>
                </div>

            </div>


        </div>
    </div>
    <div class="wa_float">
        <a href="https://bit.ly/2TE1vsv" target="_blank"><img src="{{ asset('files/wa1.png') }}"></a>
    </div>
</div>
<div class="modal fade" id="myModal"x>
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Agenda tu cita</h4>
            </div>
            <div class="modal-body" style="height: auto;text-align:center" id="agendar-modal">
                <form style="margin:auto" id='scheduleForm'>
                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                    <div class="row" style="text-align:left">
                        <div class="col-sm-12 col-lg-12 col-xl-12">
                            <h5 class="modal-title">Escribe tu nombre</h5>
                            <input type="text" name="name" style="text-align:center"/>
                        </div>
                        <div class="col-sm-4 col-lg-4 col-xl-4">
                            <h5 class="modal-title">Dejame tu numero</h5>
                            <input type="text" name="phone" style="text-align:center"/>
                        </div>
                        <div class="col-sm-4 col-lg-4 col-xl-4">
                            <h5 class="modal-title">Escoge una fecha</h5>
                            <input type="text" name="task_date" class="date" style="text-align:center;" onchange="setTimeSchedules()"/>
                        </div>
                        <div class="col-sm-4 col-lg-4 col-xl-4">
                            <h5 class="modal-title">Escoge un horario</h5>
                            <select style="text-align:center;width:70%" id="select_schedule" name="time_schedule_id">
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-close" data-dismiss="modal" id='closeAppointment'>Cerrar</button>
                <button type="button" class="btn button-agend" onclick="agendar()">Agendar</button>
                <button style="display:none" data-toggle="modal" data-target="#notifyModal" id="openNotify"></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="notifyModal" style="font-family: 'DIN Pro Condensed Regular';font-size: 1.4vw!important;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="titleAlert"></h4>
            </div>
            <div class="modal-body" style="height: auto;text-align:center" >
                <h5 class="modal-title" id="textAlert"></h5>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn button-close" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>


        <script>
            var data_schedule = JSON.parse('<?= preg_replace("/'/", " ", $time_schedules) ?>');
            var data_appointments = JSON.parse('<?= preg_replace("/'/", " ", $appointments) ?>');
        </script>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

        <script href="{{ asset('js/jquery-1.10.2.js') }}"></script>
        <script href="{{ asset('js/bootstrap.min.js') }}"></script>
        <script href="{{ asset('js/custom.js') }}"></script>

        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js'></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.js'></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/locale/es.js'></script>


        <script src="{{ asset('js/plugins/tableTech/pluginsTableTech.js')}}"></script>
        <script src="{{ asset('js/plugins/tableTech/tableTech.js')}}"></script>
        <script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"></script>

        <script src="{{ asset('js/app.js') }}"></script>


    </body>

</html>
