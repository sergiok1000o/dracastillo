<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\contact;
use App\Models\timeSchedule;
use App\Models\Task;

class MethodController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $time_schedules = timeSchedule::select('id','day', 'time_interval')->orderBy('time_interval', 'asc')->where('status',1)->get();
        $appointments = Task::select(
            'tk.id AS id',
            'tk.name AS name',
            'tk.phone AS phone',
            'tk.task_date AS task_date',
            'tk.time_schedule_id AS time_schedule_id',
            'ts.time_interval AS schedule_name'
        )
        ->from('tasks as tk')
        ->join('time_schedules AS ts','ts.id','tk.time_schedule_id')
        ->get();

        return view('welcome',compact('time_schedules','appointments'));
    }

    public function storeComment(Request $request){
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required'
        ]);
        $data = contact::create($request->all());
        $data->save();

        if($request->ajax()){
            return  response()->json([
                'data' => $data
            ]);
        }
    }

    public function storeAppointment(Request $request){
        $request->validate([
            'name' => 'required',
            'phone' => 'required',
            'task_date' => 'required',
            'time_schedule_id' => 'required'
        ]);
        $exist = Task::select('id')->where('task_date',$request['task_date'])->where('time_schedule_id',$request['time_schedule_id'])->first();
        if($exist!=null){
            return  response()->json([
                'data' => 'No possible'
            ]);
        }
        $data = Task::create($request->all());
        $data->save();

        if($request->ajax()){
            return  response()->json([
                'data' => $data
            ]);
        }

    }
    /**
     * Guardar una cita
     */
    public function storeTask(Request $request){
        $task = Task::create($request->all());
        $task->save();

        if($request->ajax()){
            return  response()->json([
                'data' => $task
            ]);
        }
    }
    /**
     * Guardar un horario
     */
    public function storeSchedule(Request $request)
    {
        $request->validate([
            'time_interval' => 'required',
            'day' => 'required'
        ]);
        $request['status']=1;
        $schedule = timeSchedule::create($request->all());
        $schedule->save();

        if($request->ajax()){
            return  response()->json([
                'data' => $schedule
            ]);
        }
    }
    /**
     * Borrar horario
     */
    public function deleteSchedule(Request $request){
        $schedule = timeSchedule::findOrFail($request['id']);
        $schedule->status=$request['status'];
        $schedule->update();

        if($request->ajax()){
            return  response()->json([
                'data' => $schedule
            ]);
        }
    }
    /**
     * Lista de comentarios
     */
    public function commentList(Request $request){
        $row = $request->get('_row');
        $search = $request->get('_search');

        $data = contact::select()
        ->orderBy('id', 'DESC')
        ->paginate($row);

        if($request->ajax()){
            return  response()->json([
                'data' => $data
            ]);
        }

    }
    /**
     * Lista de citas
     */
    public function appointmentList(Request $request){
        $row = $request->get('_row');
        $search = $request->get('_search');

        $data = Task::select(
            'tk.id AS id',
            'tk.name AS name',
            'tk.phone AS phone',
            'tk.task_date AS task_date',
            'tk.time_schedule_id AS time_schedule_id',
            'ts.time_interval AS schedule_name'
        )
        ->from('tasks as tk')
        ->join('time_schedules AS ts','ts.id','tk.time_schedule_id')
        ->orderBy('id', 'DESC')
        ->paginate($row);

        if($request->ajax()){
            return  response()->json([
                'data' => $data
            ]);
        }
    }
    /**
     * Lista de horarios
     */
    public function scheduleList(Request $request){
        $row = $request->get('_row');
        $search = $request->get('_search');

        $data = timeSchedule::select(
        )
        ->orderBy('id', 'DESC')
        ->paginate($row);
        //send the data in human words
        foreach ($data as $item) {
            if($item->status==1){
                $item->status_name='Activo';
            }
            else{

                $item->status_name='Inactivo';
            }
            if($item->day==0){
                $item->day_name='Domingo';
            }
            else if($item->day==1){
                $item->day_name='Lunes';
            }
            else if($item->day==2){
                $item->day_name='Martes';
            }
            else if($item->day==3){
                $item->day_name='Miercoles';
            }
            else if($item->day==4){
                $item->day_name='Jueves';
            }
            else if($item->day==5){
                $item->day_name='Viernes';
            }
            else{
                $item->day_name='Sabado';
            }
        }

        if($request->ajax()){
            return  response()->json([
                'data' => $data
            ]);
        }
    }

}
