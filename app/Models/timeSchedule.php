<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class timeSchedule extends Model
{
    protected $table = 'time_schedules';
    protected $fillable = [
        'day', 'time_interval', 'status'
    ];
}
