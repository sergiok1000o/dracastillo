/**
 * Función que cambia el contenido de la sección informativa
 */

function changeInfo(element){
    let name = $(element).text()
    document.getElementById("info_foto").src = "files/"+element.id+".jpg"
    let texto = textos.find(x => x.id == element.id)
    $("#info_texto").html(texto.texto)
    $('.button-med').removeClass('info_selected')
    $('#'+element.id).addClass('info_selected')

    var x = document.getElementById("myTopnav2");
    if (x.className === "topnav2") {
      x.className -= " responsive";
    } else {
      x.className = "topnav2";
    }
    $('#info_selecciona').html(name)

}

/**
 * Login fucntion
 */
function login(){
    let user = $('#loginform').find('input[name=uname]').val()
    let password = $('#loginform').find('input[name=psw]').val()
    if(user == 'dramilenacastillo@gmail.com' && password=='secret'){
        window.location.href = "/controlpanel";
    }
    else{
        alert('Tu usuario no es correcto')
    }
}

/**
 * Textos para la seccion informativa
 */
var textos =[
    {
        id:"info_acromegalia",
        texto:"La acromegalia es una enfermedad que se caracteriza por el crecimiento anormal de algunas estructuras del cuerpo humano. Produce cambios permanentes  en el cuerpo por crecimiento excesivo  especialmente de la cara y las extremidades.  Este crecimiento toma muchos años  en manifestarse y en ser notados por el paciente.<br><br> Usualmente esta enfermedad es causada por un tumor Benigno  de la glándula hipófisis.   El  tratamiento de la acromegalia requiere  cirugía   y la administración de medicamentos para inhibir la secreción de hormona de crecimiento."
    },
    {
        id:"info_colesterol",
        texto:"El colesterol es una sustancia que se encuentra en la sangre y  se necesita en   una cantidad específica para gozar de buena salud, sin embargo cuando los niveles son muy altos predisponen a las personas a tener  ataques cardiacos,  isquemia cerebral  y otros problemas de salud. Entre más alto se tenga el nivel del colesterol,  mayor será la probabilidad de  tener uno de estos problemas."
    },
    {
        id:"info_diabetes",
        texto:`Todas las células en el cuerpo requieren azúcar para funcionar. El azúcar entra a las células con la ayuda
        de una hormona que se llama INSULINA. Si no hay suficiente insulina, o si el cuerpo deja de responder a
        las acciones de la insulina, se suben los niveles de azúcar en la sangre. Esto es lo que pasa con las
        personas que tienen diabetes.<br><br>
        Existen dos tipos diferentes de diabetes. La diabetes tipo 1 la cual se caracteriza por poca insulina o
        ausencia de producción de insulina. En la diabetes tipo 2 el problema está en que las células del cuerpo
        no responden a la insulina, o que el cuerpo no produce suficiente insulina o ambos problemas.<br><br>

        La diabetes puede causar problemas muy graves en la salud, dado que produce ataques cardiacos,
        accidente cerebrovascular, enfermedad de los riñones, problemas visuales e incluso ceguera, dolor o
        pérdida de la sensibilidad de las manos y los pies. En casos más extremos puede producir amputaciones
        de dedos de los pies y otras partes del cuerpo.<br><br>
        Se requiere de un equipo multidisciplinario para lograr el control de la diabetes. Un actor principal es el
        Medico endocrinólogo el cual tiene propuestas de tratamiento y será su guía para llegar a metas de la
        glucosa.`
    },
    {
        id:"info_glandula",
        texto:`Existen diversas enfermedades relacionadas a esta glándula las cuales incluye:<br><br>
        • Hipotiroidismo ( disminución de producción de hormona tiroidea, tiroides lenta)<br><br>
        • Hipertiroidismo (exceso de  producción de  hormona tiroidea, Tiroides rápida)<br><br>
        • Tiroiditis (inflamación de la  glándula tiroides):<br>

        <div class="space">&#8227;Posterior al embarazo<br></div>
        <div class="space">&#8227;Posterior a una enfermedad viral<br></div>
        <div class="space">&#8227;Secundaria  una infección  de la glándula tiroides<br></div>
        <div class="space">&#8227;Posterior a trauma o manipulación de la glándula tiroides<br></div>
        <div class="space">&#8227;Posterior a tratamientos con Yodo radioactivo<br></div>
        <div class="space">&#8227;Inducida por Medicamentos<br></div><br>

        • Bocio (Glándula Tiroides aumentada de tamaño,  “Coto”)<br><br>
        • Nódulos  en la glándula tiroides<br><br>
        • Cáncer de la glándula tiroides
        `
    },
    {
        id:"info_paratiroides",
        texto:"Las glándulas paratiroides están localizadas en el cuello. Estas glándulas  producen la hormona Paratiroidea (PTH) la cual ayuda a controlar la cantidad de calcio en la sangre."
    },
    {
        id:"info_hiperendo",
        texto:"La Hipertensión arterial secundaria se  caracteriza por la elevación de las cifras de presión arterial  que es  secundaria a una causa externa. En el caso de la Hipertensión endocrina, se atribuye esta elevación a alteraciones Hormonales que llevan a hiper-producción de estas hormonas. Puede aparecer secundaria secreción inadecuada de hormonas producidas  en  la Glándula suprarrenal  como  por ejemplo: la aldosterona, el cortisol, la adrenalina, la noradrenalina.<br><br>También existen causas de hipertensión secundaria cuando existen otras alteraciones hormonales por fuera de la Glándula suprarrenal ; por ejemplo: el hipertiroidismo,  el uso de anticonceptivos orales entre otros."
    },
    {
        id:"info_hipertension",
        texto:"La Hipertensión arterial es una condición muy frecuente en los pacientes adultos. Se trata de un aumento de la presión que se ejerce sobre la pared de las arterias. Usualmente no causa síntomas, sin  embargo sus consecuencias son bastante graves como  enfermedades del corazón (infartos), accidente cerebrovascular (derrames) ,  enfermedad de los riñones entre otras.<br><br>La hipertensión arterial en la mayoría de los casos no puede curarse, pero si se puede controlar con medidas no farmacológicas y farmacológicas."
    },
    {
        id:"info_obesidad",
        texto:"La obesidad es una enfermedad crónica que ha  aumentado en   frecuencia  en la población adolescente y adulta siendo considerada una epidemia global.  Se trata de una acumulación de grasa excesiva en el cuerpo  que lleva a   causar problemas  metabólicos, Hormonales y Articulares.<br><br> Padecer de obesidad es un problema, dado  que aumenta el riesgo de tener  muchos problemas de salud. También  causa problemas para moverse, respirar y hacer actividades que otras personas con peso normal realizan con facilidad.<br><br>Existen medicamentos y cirugías que  pueden ayudar a perder peso, pero estos tratamientos están indicados solo para personas con problemas  importantes de peso que no han sido capaces   de perderlo a través de dieta y ejercicio. <br><br>Sin embargo, es importante  entender que estos tratamientos no reemplazan  los hábitos de vida saludable.  Las personas que se someten a estos tratamientos deben cambiar su manera de comer y ser activos, si no los resultados no son duraderos."
    },
    {
        id:"info_osteoporosis",
        texto:"La osteoporosis es una enfermedad   que debilita y adelgaza los huesos.  Las  personas con esta condición  se fracturan muy fácilmente, incluso con caídas o traumas menores.<br><br>La osteoporosis es una  enfermedad que no causa síntomas; se manifiesta usualmente cuando una  fractura ocurre.    Se puede realizar un examen llamado “Densitometría  ósea”  para saber si  esta condición está presente."
    }
];

/**
 * Calendar
 */
let eventsArray = []
data_appointments.forEach(function(element) {

    let object = {
            title : tConvert (element.schedule_name)+" - Agendado",
            start : element.task_date,
            url : ''
        }
    eventsArray.push(object)
});

$('#calendar').fullCalendar({
    lang: 'es',
    events : eventsArray
})

// ----------------- DEFINICIÓN DE IDIOMA ----------------------
// Recurso original:
// http://reviblog.net/2014/01/07/jquery-ui-datepicker-poner-el-calendario-en-espanol-euskera-o-cualquier-otro-idioma/
$.datepicker.regional['es'] = {
    closeText: 'Cerrar',
    prevText: 'Ant',
    nextText: 'Sig',
    currentText: 'Hoy',
    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
    dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
    dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
    weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''
};

$.datepicker.setDefaults($.datepicker.regional['es']);

$('.date').datepicker({
    autoclose: true,
    dateFormat: "yy-mm-dd"
});

function agendar(){
    let name = $('#agendar-modal').find('input[name=name]').val()
    let phone = $('#agendar-modal').find('input[name=phone]').val()
    let date = $('#agendar-modal').find('input[name=task_date]').val()
    let schedule = $('#agendar-modal').find('select[name=time_schedule_id]').val()

    if(name==''){
        $('#titleAlert').text('Datos incompletos')
        $('#textAlert').text('Debes escribir tu nombre')
        $('#openNotify').click()
    }
    else{
        if(phone==''){
            $('#titleAlert').text('Datos incompletos')
            $('#textAlert').text('Debes escribir tu teléfono')
            $('#openNotify').click()
        }
        else{
            if(date==''){
                $('#titleAlert').text('Datos incompletos')
                $('#textAlert').text('Debes elegir una fecha')
                $('#openNotify').click()
            }
            else{
                if(schedule==null){
                    $('#titleAlert').text('Datos incompletos')
                    $('#textAlert').text('Debes elegir un horario')
                    $('#openNotify').click()
                }
                else{
                    storeAppointment()
                }
            }
        }
    }
}

function storeAppointment(){
    let url = 'storeappointment'
    let data = $('#scheduleForm').serialize()

    $.post(url, data, function(result){
        if(result.data=='No possible'){
            $('#titleAlert').text('Imposible agendar')
            $('#textAlert').text('Ya han agendado una cita en este horario, escoge otro por favor.')
            $('#openNotify').click()
        }
        else{
            $('#titleAlert').text('Cita agendada')
            $('#textAlert').text('Tu cita ha sido agendada, por favor llega 15 minutos antes de la hora.')
            document.getElementById("scheduleForm").reset();
            $('#closeAppointment').click()
            $('#openNotify').click()
        }


    }).fail(function(errors){

    });
}
/**
 * Store contact
 */
function storeComment(){

    let url = 'storecomment'
    let data = $('#contactForm').serialize()

    $.post(url, data, function(result){

        $('#titleAlert').text('Hola!')
        $('#textAlert').text('He recibido tus datos y pronto estaré en contacto contigo.')
        $('#openNotify').click()
        document.getElementById("contactForm").reset();

    }).fail(function(errors){

        $('#titleAlert').text('Espera!')
        $('#textAlert').text('Debes introducir todos tus datos antes de enviar el mensaje.')
        $('#openNotify').click()

    });

}

function setTimeSchedules(){
    let date = $('#agendar-modal').find('input[name=task_date]').datepicker('getDate');
    var day = date.getDay();
    let options = ''

    data_schedule.forEach(function(element) {
        if(element.day==day){
            options = options + `<option value="${element.id}">${element.time_interval}</option>`
        }
    });
    if(options==''){
        $('#titleAlert').text('Día sin atención')
        $('#textAlert').text('No hay horarios de atención para este día, escoge otro por favor.')
        $('#openNotify').click()
    }
    $('#select_schedule').html(options)
}

function tConvert (time) {
    time = formatTime(time)
    // Check correct time format and split into components
    time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

    if (time.length > 1) { // If time format correct
      time = time.slice (1);  // Remove full string match value
      time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
      time[0] = +time[0] % 12 || 12; // Adjust hours
    }
    return time.join (''); // return adjusted time or original string
  }

function formatTime(time){
    time = time.split(":");
    return time[0]+":"+time[1]
}

function myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
      x.className += " responsive";
    } else {
      x.className = "topnav";
    }
  }
  function myFunction2() {
    var x = document.getElementById("myTopnav2");
    if (x.className === "topnav2") {
      x.className += " responsive";
    } else {
      x.className = "topnav2";
    }
  }
