/**
 * Variable GLOBAL {token_csrf}
 */
var token_csrf = $('meta[name=csrf-token]').attr('content');

class TableTech{

    /**
     *
     * @param {element} content Contiene todo el block donde se va a ejecutal la clase
     * @param {string} url Es la direccion donde se va a realizar la peticion
     * @param {array} table Permite definir la estructura de la tabla por medio de un array {" ", "Nombre", "Apellido"}
     * @param {string} functionOptional Permite ejecutar dicha funcion luego de traer los datos del AJAX
     *
     * @param {class} noneColumn "none-column" en los tds del template permiten ocultarlos.
     *
     * @version 1.0
     */

	constructor(content, url, table, functionOptional=false){
		this.content = content
        this.template = this.content.find('.template-list').html()
        this.loader = this.content.find('.overlay')

		this.table = table
		this.data = ''
		this.dataSelect = ''
		this.modalSelect = ''
        this.data_complete = ''

		this.xhr = ''
        this.lastCheckedListData = null
		this.checks = ''
		this.name_url = ''

		this.iconLeft = 'icon wb-chevron-left'
		this.iconRigth = 'icon wb-chevron-right'
		this.numberPage = `<button type="button" class="btn ">
			                  <i class="icon wb-refresh" aria-hidden="true"></i>
			                </button>`;


		this.form = {"url": url, "data": '', "pag":1}
		this.filter = {"search": '', "row": 10, "token": token_csrf}
		this.functionOptional = functionOptional
		this.hiddenColumn = true
		this.tablePaginate = true
		this.keepUrl = false

		this.checkFunctionEnabler = false
		this.checkFunction =  ''

		let cont = this;
		this.content.find('.filter_refresh').click(function(){ cont.refresh(); })
		this.content.find('.filter_select_1').click(function(){ cont.checkbox_select(true) })
		this.content.find('.filter_select_0').click(function(){ cont.checkbox_select(false) })
        this.content.find('.menu-select').hide()
	}
	// actualiza la url aplicando los filtros en una variable tipo GET
	addUrlTable(){
		if(!this.keepUrl){
			var sim = '?';
			var url = this.name_url;
			$.each(this.filter, function(key, value){
				if(value && key != 'token'){
					url += sim+key+'='+value
					sim = '&';
				}
			});
			history.pushState({selector: ""}, "/"+this.name_url, url)
		}
	}
	// toma las variables GET de la url y actualiza los filtros
	refreshUrl(){

		let cont = this;

		if(!this.keepUrl){
			$.each(cont.filter, function(key, value){

				if($.get(key)){
					cont.filter[key] = $.get(key)
					let valueFilter =cont.filter[key];
					$(cont.content).find('.filter_'+key+' option[value="'+$.get(key)+'"]').prop('selected', true)
					$(cont.content).find('.filter_'+key+'').val($.get(key))

					if($('.filter_'+key).prop('type') == 'button'){

						var options = $('.filter_'+key).siblings().find('a');
						let text = null
						$.each(options, function(key, value){
							if($(value).attr('value') == valueFilter){
								text = $(value).html()
							}
						})
						$(cont.content).find('.filter_'+key+'').html(text)
					}
				}
			});
		}
	}
	// verifica la variable y almacena la nueva peticion AJAX
	stateXhr(){
		if(this.xhr && this.xhr.readyState != 4){ this.xhr.abort() }
	}
	// permite seleccional las tabla donde se van a incluir los datos
	selectList(){
		return $(this.content).find('table')
	}
	// limpia la tabla invocando a .list_table
	printList(data){
		$(this.selectList()).html(data)
	}
	// anade contenido a la tabla invocando a .list_table
	appendList(data){
		$(this.selectList()).append(data)
	}
	// actualiza la variable data_send que va ser enviada en la petiion AJAX
	refreshForm(){
		let form = this.filter
		let data_send='', separ=''

		$.each(form, function(key, value){
			data_send += (separ+'_'+key+'='+value)
			separ = '&'
		})
		this.form.data = data_send
	}
	// realiza la peticion AJAX al servidor
	list(){
		let cont = this;
		this.stateXhr()

		this.xhr = $.post(this.form.url+'?page='+this.form.pag, this.form.data, function(result){
			cont.data_complete = result;
			cont.resultData(result)
		}).fail (function(result){
			cont.resultError(result)
		});
	}
	// ejecuta la infomracion que llego luego de la peticion AJAX
	resultData(result){

		let cont = this;
		this.data = result.data.data

		// lista la informacion de la tabla
		this.load_list_data()

		// oculta el paginate de la tabla
		this.tablePaginate ? this.paginate(6) : null

		// ajusta el tamano de la primera columna
		this.auto_widt_table()

		this.checkbox_addClass()
		this.checkbox_shift_add()

		// oculta el cargador
		this.loader.hide();

		// ejecuta la funcion opcional
		this.functionOptional ? self[cont.functionOptional](result) : false
	}
	// crea las funciones para los filtros
	filter_function(){
		let cont = this

		$.each(this.filter, function(key, value){

			let campo = key

			if($('.filter_'+key).length){

				if($('.filter_'+key).prop('type') == 'select-one'){
					cont.content.find('.filter_'+key).change(function(){
						cont.filter_refresh(campo, $(this).val())
					})
				}else if($('.filter_'+key).prop('type') == 'text'){
					cont.content.find('.filter_'+key).keyup(function(){
						cont.filter_refresh(campo, $(this).val())
					})
				}else if($('.filter_'+key).prop('type') == 'button'){
					let obj = cont.content.find('.filter_'+key)
					obj.siblings().find('a').click(function(){
						obj.html($(this).html())
						obj.val($(this).attr('value'))
						cont.filter_refresh(campo, obj.val())
					})
				}
			}
		})
    }

	filter_refresh(camp, value){
		this.filter[camp] = value
		this.form.pag = 1
		this.refresh()
	}
	// ejecuta los errores de la peticion AJAX
	resultError(result){
		this.loader.hide();
		this.fail_query(result)
	}
	// listDataTable
	load_list_data(){

		let cont = this
		let data = this.data_complete.data.data
		let tmp = $(this.template)

		this.data = [];
		this.printList('')
		this.content.find('.JCLRgrips').remove()

		// muestra el trbody de la tabla
		let itemTitleTable = tmp.clone()
		this.load_data_table(itemTitleTable, this.table, null, 1, this.hiddenColumn)
		this.appendList(itemTitleTable)

		$.each(data, function(key, value){

			// clona he anade la fila de la table
			let item = tmp.clone()
			cont.load_data_table(item, value, key, 2, cont.hiddenColumn)
			cont.appendList(item)

			// guarda la informacion en la variable this.data
			cont.data[key] =  new structureData(key, value)
		});

	}
	// carga los datos en el td
	load_data_table(item, data, key, type, hidden){
		// tipo 1 carga la informacion del encabezado de la tabla
		if(type == 1){
			let tds = item[0].cells;
			$.each(tds, function(i, element) {
				$(element).html(data[i])
				$(element).addClass('orderColumn')
			})
		}
		// tipo 2 carga la informacion de filas de la tabla
		else if(type == 2){

            //informacion del tr
			item.attr('data-id', data.id)
			item[0].innerHTML = item[0].innerHTML.replace('#key#', 'id_chk'+key)

			// valida si la consulta trae el campo "state_tr" para cambiar el estado del tr
			if(data.state_tr == 0 || data.state_tr == false){
				$(item).addClass('state_'+data.state_tr)
			}

			//carga los tds
			$.each(data, function(i, data) {
				item[0].innerHTML = item[0].innerHTML.replaceAll(('#_'+i+'_#'), data);
			});

            /** Format Number */
            let numberTr = $(item[0]).find('.number')
            $.each(numberTr, function(key, value){
                let currency = $(value).attr('data-currency')
                let decimals = $(value).attr('data-decimals')
                let numberFormated = formatNumber.new($(value).html(), currency, decimals);
                $(value).text(numberFormated)
            })

		}

		// si hay campos para ocultar lo hace
		hidden ? item[0].innerHTML = item[0].innerHTML.replace(('none-column'), 'hide-column') : null;
	}

	// paginations la tabla
	paginate(num){

		//Selecciona el div donde se pintaran los botones de paginacion.
		let divPaginate =  $(this.content).find('.paginate')
		divPaginate.html('')

		this.paginate_btn(num)
		var cont = this

		//funcion que se encarga de imprimir los botones.
		divPaginate.find(':button').click(function(){

			let type = $(this).data('state');
			let page = $(this).data('page');
            let pag = cont.data_complete.data.current_page;

			if(type == 0){
				pag--;
			}else if(type == 1){
				pag++;
			}else if(type == 2){
				pag = page;
			}

			cont.form.pag = pag;
			cont.refresh()
		});
	}
	// paginations crea el los botones
	paginate_btn(numRows){

		let cont = this
		let divPaginate = $(this.content).find('.paginate')
		let data = this.data_complete.data

		let num_p = data.last_page; //4
		let num_page = data.current_page;
		let for_i = 1;

		if(numRows>=num_p){
			numRows = num_p-1;
		}else{
			if((num_page-(numRows/2)) >= 1){
				if((num_page+(numRows/2)) <= num_p){
					for_i = (num_page-(numRows/2));
				}else{
					for_i = (num_p-numRows);
				}
			}
		}

		cont.paginate_print_btn(divPaginate, 0, (data.current_page-1), 'Anterior', 1, this.iconLeft, data)
		for (var i = for_i; i <= for_i+numRows; i++) {
			cont.paginate_print_btn(divPaginate, 2, i, ('Pagina '+i), 0, i, data)
		}
		cont.paginate_print_btn(divPaginate, 1, (data.current_page+1), 'Siguiente', 1, this.iconRigth, data)

		let tot = $(this.content).find('.paginate-total')
		tot.text('Total: '+data.total);

		divPaginate.click(function(){
			$('html,body').animate({
			scrollTop: $(cont.content).offset().top -140}, 250);
		})
	}
	// paginations print btn
	paginate_print_btn(div, state, page, title, type, value, data){

		let temp = this.numberPage;
		let btn = $(temp).clone();

		btn.attr('data-state', state);
		btn.attr('data-page', page);
		btn.attr('title', title);

		if(type == 1){
			btn.find('i').addClass(value);
		}else{
			btn.text(value);
		}

		state == 0 ? btn.addClass('btn-previous') : ''
		state == 1 ? btn.addClass('btn-next') : ''

		if(state == 0)
			data.current_page == 1 ? btn.attr("disabled", true) : ''

		if(state == 1)
			data.current_page == data.last_page ? btn.attr("disabled", true) : ''

		if(state == 2)
			data.current_page == page ? btn.addClass('btn-primary') : ''

		$(div).append(btn)
    }

    /**
     * @description show errors in notifications
     * @param {Array} errors
     */
    fail_query(errors){
        errors.status == 500 ? notify(3) : null
        errors.status == 404 ? notify(1) : null
        errors.status == 422 ? notify(4) : null
    }

    /**
     * @description Apply css to checkboox
     */
    checkbox_addClass(){
        $(this.content).find('.checkbox-icheck').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_minimal',
            increaseArea: '20%'
        });
    }

    /**
     * @description Function Select all (true/false)
     */
    checkbox_select(state){

        let cont = 0;
        let content = $(this.selectList())
        let checkGroup = content.find('input:checkbox')
        let c_everyboby = content.find('.check_everyboby');
        let default_tr = checkGroup.parent().parent().parent()

        state ? checkGroup.iCheck('check') : checkGroup.iCheck('uncheck')
        state ? default_tr.addClass('active') : default_tr.removeClass('active')
        state ? c_everyboby.prop('checked', true) : c_everyboby.prop('checked', false)

        this.checkbox_count()
    }

    /**
     * @description saves the checks of the list in this.data
     */
    checkbox_count(){

        let checkGroup = this.selectList().find('input:checkbox')
        this.checks = [];
        this.checks.length = 0;

        let class_tr = this
        let cont = 0;
        for (let x=0; x < checkGroup.length; x++) {
            if (checkGroup[x].checked) {
                class_tr.checks[cont] = $(checkGroup[x]).parent().parent().parent().data('id');
                cont = cont + 1;
            }
        }

		cont ? this.content.find('.menu-select').show() : this.content.find('.menu-select').hide()

		this.checkFunctionEnabler ?  self[this.checkFunction](class_tr.checks) : null
    }

    /**
     * @description Select one and select shiftkey of the list (Check)
     */
    checkbox_shift_add(){

        let div = this.selectList()
        let chkboxes = div.find('.iCheck-helper')
        let class_tr = this

        $(chkboxes).click(function(e) {

            let checkGroup = div.find('input:checkbox')
            let check = $(this).siblings('input:checkbox')
            let tr_default = check.parent().parent().parent()


            check.is(":checked") ? tr_default.addClass('active') : tr_default.removeClass('active')

            if(!class_tr.lastCheckedListData) {
                class_tr.lastCheckedListData = check
                class_tr.checkbox_count()
                return
            }

            if(e.shiftKey) {
                let start = checkGroup.index(check)
                let end = checkGroup.index(class_tr.lastCheckedListData)

                let sliceCheck = checkGroup.slice(Math.min(start,end), Math.max(start,end)+ 1)

                for (let i = sliceCheck.length - 1; i >= 0; i--) {

                    let default_check = $(checkGroup.slice(Math.min(start,end), Math.max(start,end)+ 1)[i])
                    let default_tr = default_check.parent().parent().parent()

                    check.is(":checked") ? default_tr.addClass('active') : default_tr.removeClass('active')
                    check.is(":checked") ? default_check.iCheck('check') : default_check.iCheck('uncheck')
                }
            }

            class_tr.lastCheckedListData = check;
            class_tr.checkbox_count()
        });
    }
    /**
     * @description Function Select all (true/false)
     */
    change_row(element){
        let rows = $(element).html();
        this.filter.row = rows;
        this.form.pag = 1
        this.refresh();
    }

    /**
     * @description Execute Action from the buttons
     */
    modal(element){

        let tr = $(element).parent().parent().index()
        let action = $(element).data('action')

        this.modalSelect = $($(element).data('modal'))
        this.dataSelect = tr ? this.data[tr-1].data : 0

        this.modalSelect.modal('show');
        this.modal_loader(false);

        action ? self[action]() : false;

        let cont = this;
        this.modalSelect.find('.close').click(function(){
        	cont.modal_close();
        })
    }

    /**
     * @param {boolean} status
     * @returns change modal loader status
     */
    modal_loader(status){
        status ? this.modalSelect.find('.overlay').show() : this.modalSelect.find('.overlay').hide()
    }

    /**
     * @description Basic functions of modal closure
     * @param {form} form modals list
     */
    modal_close(form = null){
        form ? form[0].reset() : null
        this.modal_loader(false)
        this.modalSelect.modal('hide')

        form ? this.modal_clear_form(form) : null
    }

    modal_clear_form(form){
    	let input = form.find('input')
        let select = form.find('select')
        let textarea = form.find('textarea')
        input.removeClass('is-invalid')
        select.removeClass('is-invalid')
        textarea.removeClass('is-invalid')
        input.parent().find('.error').html('')
        select.parent().find('.error').html('')
    }

    modal_validate_inputs(errors, form){

        this.modal_clear_form(form)
        $.each(errors, function(key, value){

            let input = form.find('input[name='+key+']')
            let select = form.find('select[name='+key+']')
            let textarea = form.find('textarea[name='+key+']')
            let input_list = input.parent().parent().find('.dataSelect').find('input[name=search]')

            input.addClass('is-invalid')
            select.addClass('is-invalid')
            input_list.addClass('is-invalid')
            textarea.addClass('is-invalid')

            input.parent().find('.error').html('<i class="icon wb-alert-circle"></i> '+value)
            select.parent().find('.error').html('<i class="icon wb-alert-circle"></i> '+value)
            textarea.parent().find('.error').html('<i class="icon wb-alert-circle"></i> '+value)
        })
    }

    modal_load_form(){
    	let cont = this;
    	$.each(this.dataSelect, function(key, value){
		    cont.modalSelect.find('form').find('input[name='+key+']').val(value);
			cont.modalSelect.find('form').find('textarea[name='+key+']').val(value);
  		})
    }

    /**
     *
     * @param {Element} select Example: $('.select')
     * @param {array} data Structure {id: 1, name: Pepe}
     * @param {int|string} selected
     * @param {String} name_default Default name of the first option
     */
    loadSelect(select, data, selected, name_default){

        let d = data;
        $(select).find('option').remove();

        if(name_default){
            $(select).append('<option data-alt="" value="" class="c_gray_cl">'+name_default+'</option>');
            }else{
            $(select).append('<option data-alt="" value="" class="c_gray_cl">Selecciona...</option>');
            }

        $.each(d, function (key,value){
            $(select).append($('<option>', {value: d[key].id, text: d[key].name}));
        });

        selected ? select.find('option[value="'+selected+'"]').attr('selected', 'selected') : ''
    }

	/**
     *
     * @param {Element} radio The html div where is going to be displayed the radio options Example: $('.radio')
     * @param {array} data The array of the objects to be displayed, it must has id and name Structure {id: 1, name: Pepe}
     * @param {int|string} selected The pre defined value of the radio button
     * @param {String} dataField The field of the database related to the input, it must be exactly as in the database
     */
    loadRadio(radio, data, selected, dataField){
		let d = data;
		$(radio).find('.radio-custom').remove();

		$.each(d, function (key,value){
			let radioOption=`<div class="radio-custom radio-default radio-inline">
								<input type="radio" name="${dataField}" value="${value.id}"/>
								<label for="inputBasicMale">${value.name}</label>
							</div>`;
			$(radio).append(radioOption);
        });

		selected ? radio.find('input[value="'+selected+'"]').attr('checked', 'true') : ''
	}

    /**
     *
     * @param {Element} radio The html div where is going to be displayed the radio options Example: $('.radio')
     * @param {array} data The array of the objects to be displayed, it must has id and name Structure {id: 1, name: Pepe}
     * @param {int|string} selected The pre defined value of the radio button
     * @param {String} dataField The field of the database related to the input, it must be exactly as in the database
     */
    loadCheck(check, selected){

        selected ? check.attr('checked',true) : check.attr('checked',false)

	}

	// ajusta tamano de las columnas 1 de la tabla
	auto_widt_table(){

		let div = this.selectList().find('tr')
		var td = $(div[1]).find('td')[0]

		let cont = $(td).find('i').length + $(td).find('input').length
		let camp;

		switch(cont){
			case 1: camp = 55; break;
			case 2: camp = 40; break;
			case 3: camp = 37; break;
			case 4: camp = 36; break;
			default: camp = 33; break;
		}

		cont ? $(div[0]).find('td')[0].width = (cont*camp)+'px' : null
	}
	// ejecuta la CLASS
	refresh(state = false){

		state ? this.filter_function() : null
		state ? this.refreshUrl() : null

		this.addUrlTable()
		this.refreshForm()
        this.list()
        this.checks = null

		this.content.find('.menu-select').hide()
		this.loader.show();
	}
	getDataById(idArray){
		let ansArray = [];
		$.each(this.data, function (key,value){
			if(idArray.find( x => x === value.data.id )){
				ansArray.push(value.data)
			}
		});
		return ansArray
	}
}
// END tableGerData


// ----------------------------------------------------------------------------------------------------------------------------------------------------------------

/**
 *
 * @param {Element} select Example: $('.select')
 * @param {array} data Structure {id: 1, name: Pepe}
 * @param {int|string} selected
 * @param {String} name_default Default name of the first option
 */
function loadSelect(select, data, selected, name_default)
{
    let d = data;
    $(select).find('option').remove();

    if(name_default){
        $(select).append('<option data-alt="" value="" class="c_gray_cl">'+name_default+'</option>');
        }else{
        $(select).append('<option data-alt="" value="" class="c_gray_cl">Selecciona...</option>');
        }

    $.each(d, function (key,value){
        $(select).append('<option data-alt="'+d[key].alt+'" value="'+d[key].id+'">'+d[key].name+'</option>');
    });

    selected ? select.find('option[value="'+selected+'"]').attr('selected', 'selected') : ''
}

/**
 * Other Functions
 * @name GET attribute URL
 */
(function($) {
    $.get = function(key)   {
        key = key.replace(/[\[]/, '\\[');
        key = key.replace(/[\]]/, '\\]');
        var pattern = "[\\?&]" + key + "=([^&#]*)";
        var regex = new RegExp(pattern);
        var url = unescape(window.location.href);
        var results = regex.exec(url);
        if (results === null) {
            return null;
        } else {
            return results[1];
        }
    }
})(jQuery);

/**
 * @description Structure data
 */
class structureData{
	constructor(num, data){
		this.num = num
		this.data = data
	}
}

/**
 *
 * @param {*} type
 * @param {*} title
 * @param {*} text
 * @param {*} numType
 * @param {int|min=1|max=4} position
 */
function notify(type, title=null, text=null, numType=null, position=1){

	var msnType = [
	    {"name":"info", "icono":"icon fa fa-info"},
	    {"name":"danger", "icono":"icon fa fa-ban"},
	    {"name":"success", "icono":"icon fa fa-check"},
	    {"name":"warning", "icono":"icon fa fa-warning"}
	]

	var notifyDefault = [
	    {"title":"defaul", "text":"", "numType":"1"},
	    {"title":"Comprueba tu Conexión", "text":"No se puede conectar a la base de datos", "numType":"1"},
	    {"title":"La operación se realizo con éxito", "text":"", "numType":"2"},
	    {"title":"Error Controller", "text":"No se puede recibir información del controlador", "numType":"1"},
	    {"title":"", "text":"Problemas al validar los campos", "numType":"1"},
	]

	if(type){
		title = notifyDefault[type].title
		text = notifyDefault[type].text
		numType = notifyDefault[type].numType
	}

	positionDev = { from: "bottom", align: "right" }
	if(position == 2){
		positionDev = { from: "bottom", align: "left" }
	}else if(position == 3){
		positionDev = { from: "top", align: "right" }
	}else if(position == 4){
		positionDev = { from: "top", align: "left" }
	}

	$.notify({
		icon: msnType[numType].icono,
		title: title,
		message: text,
	},{
		element: 'body',
		position: null,
		type: msnType[numType].name,
		allow_dismiss: true,
		newest_on_top: false,
		showProgressbar: false,
		placement: positionDev,
		offset: 20,
		spacing: 10,
		z_index: 1800,
		delay: 3000,
		timer: 100,
		url_target: '_blank',
		mouse_over: null,
		animate: {
			enter: 'animated fadeInDown',
			exit: 'animated fadeOutUp'
		},
		onShow: null,
		onShown: null,
		onClose: null,
		onClosed: null,
		icon_type: 	'class',
        template: 	'<div class="alert alert-success alert-dismissible"></div>'+
                        '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
						    '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
						    '<span data-notify="icon"></span>'+
						    '<span data-notify="title"><strong>{1}</strong></span> ' +
						    '<span data-notify="message">{2}</span>' +
                            '<div class="progress" data-notify="progressbar">' +
                                '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                            '</div>' +
						    '<a href="{3}" target="{4}" data-notify="url"></a>' +
                        '</div>'
	});
}

/**
 * This function gets the ids from the multiple select view.
 *
 * @param {html} form The form where is the multiple select input
 * @param {array} comparisonInfo The vector of data to compare the values displayed and get the ids
 *
 * @returns {array} The vector of the ids of the options selected
 */
function getIdsFromForm(form, comparisonInfo){
  let htmlDivs = form.find('.ms-selection').find('li')
  let assignedNames = [];
  let assignedIds = [];
  $.each(htmlDivs, function(key, value){
    if($(value).hasClass('ms-selected')){
      assignedNames.push($(value).find('span').html())
    }
  })
  $.each(comparisonInfo, function(key, value){
    if(assignedNames.includes(value.name)){
      assignedIds.push(value.id)
    }
  })
  return assignedIds;
}

/**
 * This function groups an array by a key.
 *
 * @param {array} array The array of the data that is going to be grouped
 * @param {string} key The key value by which the aray is going to be grouped
 *
 * @returns {array} The array grouped by the keys
 */
function groupBy(array, key) {

	return array.reduce(function(rv, x) {

	  (rv[x[key]] = rv[x[key]] || []).push(x);
	  return rv;

	}, {});

};



let location_es = {
    "format": "YYYY-MM-DD",
    "separator": " - ",
    "applyLabel": "Aplicar",
    "cancelLabel": "Cancelar",
    "fromLabel": "From",
    "toLabel": "To",
    "customRangeLabel": "Custom",
    "daysOfWeek": [
        "Dom",
        "Lun",
        "Mar",
        "Mie",
        "Jue",
        "Vie",
        "Sab"
    ],
    "monthNames": [
        "Enero",
        "Febrero",
        "Marzo",
        "Abril",
        "Mayo",
        "Junio",
        "Julio",
        "Agosto",
        "Septiembre",
        "Octubre",
        "Noviembre",
        "Diciembre"
    ],
    "firstDay": 1
};

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};

/**
 * Function to format number as currency
 */
function number_format(amount, decimals) {

    amount += '';
    amount = parseFloat(amount.replace(/[^0-9\.]/g, ''));

    decimals = decimals || 0;
    if (isNaN(amount) || amount === 0)
        return parseFloat(0).toFixed(decimals);

    amount = '' + amount.toFixed(decimals);
    var amount_parts = amount.split('.'),
        regexp = /(\d+)(\d{3})/;

    while (regexp.test(amount_parts[0]))
        amount_parts[0] = amount_parts[0].replace(regexp, '$1' + '.' + '$2');

    return amount_parts.join('.');
}

var formatNumber = {
    separador: ".",
    sepDecimal: ',',
    formatear:function (num){
        num = parseFloat(num).toFixed(this.decimals)
        num +='';
        var splitStr = num.split('.');
        var splitLeft = splitStr[0];
        var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
        var regx = /(\d+)(\d{3})/;

            while (regx.test(splitLeft)) {
                splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
            }
            return this.simbol + ' ' + (splitLeft + splitRight)
    },
    new:function(num, simbol, decimals){
        this.simbol = simbol ||'';
        this.decimals = decimals ||'';
        return this.formatear(num);
    }
}


/** valida si el campo es numerico y no esta nulo */
function validateNumber(data, min=0, max=9999999,)
{
    let s=0
    if(isNaN(data) || data < min || data > max){
        s++
    }else if(data==null || data==""){
        s++
    }
    return !s ? true : false
}


/** show|hide loader modal */
function loaderContent(modal, s=false)
{
    s ? modal.find('.overlay-modal').show() :  modal.find('.overlay-modal').hide()
}


// this class show data for select searchs by cards from ajax service app
class selectTech{

    /**
     *
     * @param {string} selectClass Contiene el selector via class de un elemento select
     * @param {string} url Ruta para el servicio ajax
     * @param {string} renderCard Diño y parametros de la vista render car
     *
     * @version 1.0
     */

    constructor(container, options){

        this.container = container
        this.url = ''
        this.method = ''
        this.id = ''
        this.rowPage = 30
        this.placeholder = 'Buscar'
        this.functionAlt = null

        this.element = null

        let cont = this
        $.each(options, function(key, value){
            cont[key] = value
        })

        this.selectCard();
    }
    selectCard(){
        let cont  = this

        this.element = this.container.select2({
            language: "es",
            ajax: {
                url: cont.url,
                dataType: 'json',
                method: cont.method,
                delay: 300,
                data: function (params) {
                    return {
                        _search: params.term,
                        _id: cont.id,
                        _token : token_csrf
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: data.data.data,
                        pagination: {
                            more: (params.page * cont.rowPage) < data.data.total
                        }
                    };
                },
                cache: true
            },
            // placeholder: cont.placeholder,
            /** let our custom formatter work */
            escapeMarkup: function (markup) {
                return markup;
            },
            SingleSelection: true,
            minimumInputLength: 1,
            templateResult: cont.formatRepo,
            templateSelection: cont.formatRepoSelection
        });
        this.clickSelect()

    }
    clickSelect(){
    }
    formatRepo(data){

        let cont = this

        if (data.loading) {
            return "Espera";
        }

        var markup =    `<div class="media">
                            <div class="pr-20">
                                <a class="avatar" href="javascript:void(0)">
                                    <img class="img-fluid" src="${data.avatar}">
                                </a>
                            </div><div class="media-body">
                                <h5 class="mt-0 mb-5 hover">${data.name}</h5>
                                <small>${data.details}</small>
                            </div>
                        </div>`;

        return markup;
    }
    formatRepoSelection(data){
        resultSelect(data)
    }
}
