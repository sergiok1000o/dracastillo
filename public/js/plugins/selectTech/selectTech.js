// this class show data for select searchs by cards from ajax service app

class selectTech{

    /**
     *
     * @param {string} selectClass Contiene el selector via class de un elemento select
     * @param {string} url Ruta para el servicio ajax
     * @param {string} renderCard Diño y parametros de la vista render car
     *
     * @version 1.0
     */

    constructor(container, options){

        this.container = container
        this.url = ''
        this.method = ''
        this.id = ''
        this.rowPage = 30
        this.placeholder = 'Buscar'
        this.functionAlt = null

        let cont = this
        $.each(options, function(key, value){
            cont[key] = value
        })
    }
    selectCard(){
        let cont  = this

        this.container.select2({
            ajax: {
                url: cont.url,
                dataType: 'json',
                method: cont.method,
                delay: 300,
                data: function (params) {
                    return {
                        _search: params.term,
                        _id: cont.id,
                        _token : token_csrf
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: data.data.data,
                        pagination: {
                            more: (params.page * cont.rowPage) < data.data.total
                        }
                    };
                },
                cache: true
            },
            placeholder: cont.placeholder,
            /** let our custom formatter work */
            escapeMarkup: function (markup) {
                return markup;
            },
            minimumInputLength: 1,
            templateResult: cont.formatRepo,
            templateSelection: cont.formatRepoSelection
        });

    }
    formatRepo(data){
        if (data.loading) {
            return data.text;
          }

        var markup =    `<div class="media">
                            <div class="pr-20">
                                <a class="avatar" href="javascript:void(0)">
                                    <img class="img-fluid" src="${data.avatar}">
                                </a>
                            </div>
                            <div class="media-body">
                                <h5 class="mt-0 mb-5 hover">${data.name}</h5>
                                <small>${data.details}</small>
                            </div>
                        </div>`;

        return markup;
    }
    formatRepoSelection (data) {
        this.functionAlt ? self[this.functionAlt](data) : null
    }
}



