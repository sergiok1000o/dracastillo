/**
 * Table creation for the index view.
 *
 * First the tableTech class is initialized with the following variables:
 * @param {html} div The div where the table is going to be displayed
 * @param {string} route The url address where the table is going to consult the information
 * @param {array} structure The structure of the table previously defined
 *
 * @returns {TableTech} Returns the TableTech object
 *
 * Second the table is refreshed to fill the information for the first time
 */
let div = $('#tableComments')
let route = '/comments/list'
let structure = ["Nombre", "Correo", "Teléfono","Comentario", "Fecha"]

var commentsTable = new TableTech(div, route, structure)
commentsTable.refresh(true)

let divA = $('#tableAppointments')
let routeA = '/appointments/list'
let structureA = ["Paciente", "Teléfono", "Fecha","Horario"]

var appointmentsTable = new TableTech(divA, routeA, structureA)
appointmentsTable.refresh(true)

let divB = $('#tableSchedules')
let routeB = '/schedules/list'
let structureB = ["Día", "Horario","Estado",""]

var schedulesTable = new TableTech(divB, routeB, structureB)
schedulesTable.refresh(true)

/**
 * Change window
 */
function changeWindow(id){
    $('#mensajes-link').removeClass('active-link')
    $('#citas-link').removeClass('active-link')
    $('#horarios-link').removeClass('active-link')
    $('#mensajes').addClass('d-none')
    $('#citas').addClass('d-none')
    $('#horarios').addClass('d-none')

    $('#'+id+'-link').addClass('active-link')
    $('#'+id).removeClass('d-none')
}

$("#datetimepicker3").datetimepicker({
    format: 'HH:mm'
});

/**
 * Store schedule
 */
function storeSchedule(){

    let url = 'storeschedule'
    let data = $('#scheduleForm').serialize()

    $.post(url, data, function(result){
        $('#modalCreateSchedule').modal('hide');
        schedulesTable.refresh()
        alert('Se ha creado el horario')
        document.getElementById("contactForm").reset();

    }).fail(function(errors){
        alert('No se ha creado el horario')
    });

}

function deleteSchedule(id, status){

    let url = 'deletechedule'
    let data = {
        id: id,
        status: status,
        _token:$('meta[name=csrf-token]').attr('content')
    }

    $.post(url, data, function(result){
        alert('Se cambio el estado del horario')
        schedulesTable.refresh()

    }).fail(function(errors){
        alert('No se ha podido cambiar el estado del horario')
    });

}
